import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import "./AboutCountry.css";

const AboutCountry = (props) => {
  const [info, setInfo] = useState(null);
  const [borders, setBorders] = useState([]);

  const fetchData = useCallback(async () => {
    if (props.id !== null) {
      const infoResponse = await axios.get("alpha/" + props.id);
      let resultBordes = [];
      for (let item of infoResponse.data.borders) {
        const result = await axios.get("alpha/" + item);
        resultBordes.push(result.data);
      }
      setInfo(infoResponse.data);
      setBorders(resultBordes);
    }
  }, [props.id]);

  useEffect(() => {
    fetchData().catch(console.error);
  }, [props.id, fetchData]);

  return (
    info && (
      <div className="AboutCountry">
        <h1>Названия:{info.name}</h1>
        <p>Столица:{info.capital}</p>
        <img src={info.flag} alt="Flag" />
        <div>
          {borders.map((border) => (
            <div key={border.name}>
              <p>Граничит с : {border.name}</p>
            </div>
          ))}
        </div>
        <div>
          {info.currencies.map((currencies) => (
            <div key={currencies.code}>
              <p>Валютa:{currencies.code}</p>
              <p> Названия:{currencies.name}</p>
              <p>Символ:{currencies.symbol}</p>
            </div>
          ))}
        </div>
      </div>
    )
  );
};

export default AboutCountry;
