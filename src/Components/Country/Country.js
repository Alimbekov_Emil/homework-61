import React from "react";
import "./Country.css";

const Country = (props) => {
  return (
    <button className="Country" onClick={() => props.clicked(props.code)}>
      {props.number}. {props.name}
    </button>
  );
};

export default Country;
