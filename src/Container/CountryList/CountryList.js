import React, { useEffect, useState } from "react";
import axios from "axios";
import Country from "../../Components/Country/Country";
import "./CountryList.css";
import AboutCountry from "../../Components/AboutCountry/AboutCountry";

const CountryList = () => {
  const [allCountry, setAllCounry] = useState([]);
  const [alphaCode, setAlphaCode] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get("all?fields=name;alpha3Code");
      setAllCounry(response.data);
    };

    fetchData().catch(console.error);
  }, []);

  return (
    <div className="CountryList">
      <div className="Сountries">
        {allCountry.map((country, index) => (
          <Country
            key={country.name + index}
            clicked={setAlphaCode}
            name={country.name}
            number={index + 1}
            code={country.alpha3Code}
          />
        ))}
      </div>
      <div className="CountryInfo">
        <AboutCountry id={alphaCode} />
      </div>
    </div>
  );
};

export default CountryList;
